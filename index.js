const fs = require('fs');
const _ = require('lodash');

fs.exists('eslintrc.merge.json', (exists) => {
  let user = exists ? JSON.parse(fs.readFileSync('eslintrc.merge.json')) : {};

  let eslintrc = _.merge(JSON.parse(fs.readFileSync('node_modules/adapptor-lint-config/eslintrc.json')), user);

  fs.writeFile('.eslintrc.json', JSON.stringify(eslintrc, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('Successfully wrote eslint config to .eslintrc.json');
  });
});

fs.exists('eslintignore.merge.ignore', (exists) => {
  let user = exists ? fs.readFileSync('eslintignore.merge.ignore').toString() : '';

  let eslintignore = fs.readFileSync('node_modules/adapptor-lint-config/eslintignore.ignore').toString();

  fs.writeFile('.eslintignore', eslintignore + user, (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('Successfully wrote eslint ignore file to .eslintignore');
  });
});

fs.exists('tsconfig.merge.json', (exists) => {
  let user = exists ? JSON.parse(fs.readFileSync('tsconfig.merge.json')) : {};

  let tsconfig = _.merge(JSON.parse(fs.readFileSync('node_modules/adapptor-lint-config/tsconfig.json')), user);

  // Only default to "index.js" if neither "files" nor "include" are specified
  if (!tsconfig.files && !tsconfig.include) {
    tsconfig.files = ["index.js"];
  }

  fs.writeFile('tsconfig.json', JSON.stringify(tsconfig, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('Successfully wrote typescript config file to tsconfig.json');
  });
});
